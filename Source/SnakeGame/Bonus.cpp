// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BonusMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusMesh"));

}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector NewLocation = GetActorLocation();
	float RunningTime = GetGameTimeSinceCreation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
	NewLocation.Z += DeltaHeight * 10.0f;
	SetActorLocation(NewLocation);
}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			APlayerPawnBase* SnakePawn = GetWorld()->GetPlayerControllerIterator()->Get(0)->GetPawn<APlayerPawnBase>();
			if (BonusMesh->GetMaterial(0) == SnakePawn->SpeedUpMaterial) {
				if (Snake->MovementSpeed > 0.25f) {
					Snake->MovementSpeed -= 0.05f;
					Snake->SetActorTickInterval(Snake->MovementSpeed);
					SnakePawn->GameInfo = "Snake speed increased";
				}
			}
			else if (BonusMesh->GetMaterial(0) == SnakePawn->SpeedDownMaterial) {
				Snake->MovementSpeed += 0.05f;
				Snake->SetActorTickInterval(Snake->MovementSpeed);
				SnakePawn->GameInfo = "Snake speed reduced";
			}
			else if (BonusMesh->GetMaterial(0) == SnakePawn->ThroughWallsMaterial) {
				if (Snake->bPassesThroughWalls) {
					Snake->bPassesThroughWalls = false;
					SnakePawn->GameInfo = "Snake do not passes through the walls";
				}
				else {
					Snake->bPassesThroughWalls = true;
					SnakePawn->GameInfo = "Snake passes through the walls";
				}
			}
		}
		this->Destroy();
	}
}

