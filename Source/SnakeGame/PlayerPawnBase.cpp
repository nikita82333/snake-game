// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Food.h"
#include "Wall.h"
#include "Bonus.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

	SnakeGameMode = EGameMode::MAIN_MENU;
	SnakeElementSize = 60.f;
	SnakeElementSpace = 20.f;
	SnakeMovementSpeed = 0.5f;
	SnakeStartLenght = 5;
	FieldSizeX = 17;
	FieldSizeY = 25;
	BaseFoodsNumber = 15;
	BaseWallsNumber = 10;
	BaseBonusesNumber = 5;
	FoodsNumber = BaseFoodsNumber;
	WallsNumber = BaseWallsNumber;
	BonusesNumber = BaseBonusesNumber;
	Score = 0;
	TotalScore = 0;
	Level = 1;
	bIsGameRestart = false;
	bIsNextLevel = false;
	MainMenuWinLost = "Main menu";
	StartGameButtonLabel = "Start game";
	Map = new EMapElementType * [FieldSizeX];
	for (int i = 0; i < FieldSizeX; ++i) {
		Map[i] = new EMapElementType[FieldSizeY];
	}
}

APlayerPawnBase::~APlayerPawnBase()
{
	for (int i = 0; i < FieldSizeX; ++i) {
		delete[] Map[i];
	}
	delete[] Map;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	//StartGame();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAxis("Pause", this, &APlayerPawnBase::HandlePlayerPauseInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (SnakeGameMode == EGameMode::GAME) {
			if (value > 0) {
				SnakeActor->NewMoveDirection = EMovementDirection::UP;
			}
			else if (value < 0) {
				SnakeActor->NewMoveDirection = EMovementDirection::DOWN;
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (SnakeGameMode == EGameMode::GAME) {
			if (value > 0) {
				SnakeActor->NewMoveDirection = EMovementDirection::RIGHT;
			}
			else if (value < 0) {
				SnakeActor->NewMoveDirection = EMovementDirection::LEFT;
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerPauseInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value == 10 && SnakeGameMode != EGameMode::MAIN_MENU) {
			SnakeActor->bPauseGame = !SnakeActor->bPauseGame;
			if (SnakeActor->bPauseGame) {
				SnakeGameMode = EGameMode::PAUSE;
			} else {
				SnakeGameMode = EGameMode::GAME;
			}
		}
	}
}

void APlayerPawnBase::CreateGameMap()
{
	//map initialisation
	for (int i = 0; i < FieldSizeX; ++i) {
		for (int j = 0; j < FieldSizeY; ++j) {
			Map[i][j] = EMapElementType::NONE;
		}
	}
	//clear column for snake (and for its initial movement)
	for (int i = 0; i < FieldSizeX; ++i) {
		Map[i][FieldSizeY / 2] = EMapElementType::SNAKE;
	}
	//foods
	for (int i = 0; i < FoodsNumber; ++i) {
		int X, Y;
		GetRandomEmptyCell(X, Y);
		Map[X][Y] = EMapElementType::FOOD;
	}
	//barriers
	for (int i = 0; i < WallsNumber; ++i) {
		int X, Y;
		GetRandomEmptyCell(X, Y);
		Map[X][Y] = EMapElementType::WALL;
	}
	//bonuses
	for (int i = 0; i < BonusesNumber; ++i) {
		int Type = FMath::RandRange(1, 3);
		int X, Y;
		GetRandomEmptyCell(X, Y);
		switch (Type)
		{
		case 1:
			Map[X][Y] = EMapElementType::B_SPEED_UP;
			break;
		case 2:
			Map[X][Y] = EMapElementType::B_SPEED_DOWN;
			break;
		case 3:
			Map[X][Y] = EMapElementType::B_THROUGH_WALLS;
			break;
		default:
			break;
		}
	}
}

void APlayerPawnBase::CreateGameElements()
{
	float Cell{ SnakeElementSize + SnakeElementSpace };
	for (int i = 0; i < FieldSizeX; ++i) {
		for (int j = 0; j < FieldSizeY; ++j) {
			float X = (-FieldSizeX / 2 + i) * Cell;
			float Y = (-FieldSizeY / 2 + j) * Cell;
			switch (Map[i][j])
			{
			case EMapElementType::FOOD:
				AddFood(X, Y);
				break;
			case EMapElementType::WALL:
				AddWall(X, Y, 0, true);//add barriers
				break;
			case EMapElementType::B_SPEED_UP:
				AddBonus(X, Y, SpeedUpMaterial);
				break;
			case EMapElementType::B_SPEED_DOWN:
				AddBonus(X, Y, SpeedDownMaterial);
				break;
			case EMapElementType::B_THROUGH_WALLS:
				AddBonus(X, Y, ThroughWallsMaterial);
				break;
			default:
				break;
			}
		}
	}
}

void APlayerPawnBase::CreateWalls()
{
	float Cell{ SnakeElementSize + SnakeElementSpace };
	for (int i = 0; i < FieldSizeX + 2; ++i) {
		for (int j = 0; j < FieldSizeY + 2; ++j) {
			float X = (-(FieldSizeX + 2) / 2 + i) * Cell;
			float Y = (-(FieldSizeY + 2) / 2 + j) * Cell;
			AddWall(X, Y, -Cell, false);//add boundary walls
			if (i == 0 || i == FieldSizeX + 1 || j == 0 || j == FieldSizeY + 1) {
				AddWall(X, Y, 0, false);
			}
		}
	}
}

void APlayerPawnBase::AddFood(float X, float Y)
{
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform(NewLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}

void APlayerPawnBase::AddWall(float X, float Y, float Z, bool bIsBarrier)
{
	FVector NewLocation(X, Y, Z);
	FTransform NewTransform(NewLocation);
	AWall* NewWall = GetWorld()->SpawnActor<AWall>(WallClass, NewTransform);
	if (bIsBarrier)
		NewWall->WallMesh->SetMaterial(0, BarriersMaterial);
}

void APlayerPawnBase::AddBonus(float X, float Y, UMaterial* Material)
{
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform(NewLocation);
	ABonus* NewBonus = GetWorld()->SpawnActor<ABonus>(BonusClass, NewTransform);
	NewBonus->BonusMesh->SetMaterial(0, Material);
}

void APlayerPawnBase::GetRandomEmptyCell(int& X, int& Y)
{
	X = FMath::RandRange(0, FieldSizeX - 1);
	Y = FMath::RandRange(0, FieldSizeY - 1);
	while (Map[X][Y] != EMapElementType::NONE) {
		X = FMath::RandRange(0, FieldSizeX - 1);
		Y = FMath::RandRange(0, FieldSizeY - 1);
	}
}

void APlayerPawnBase::StartGame()
{
	GameInfo = "";
	Score = 0;
	if (bIsGameRestart) {
		SnakeActor->DestroySnake();
		DestroyGameElements();
	} else {
		CreateWalls();
	}
	if (bIsNextLevel) {
		FoodsNumber++;
		WallsNumber++;
		BonusesNumber++;
		Level++;
	}
	else {
		FoodsNumber = BaseFoodsNumber;
		WallsNumber = BaseWallsNumber;
		BonusesNumber = BaseBonusesNumber;
		Level = 1;
		TotalScore = 0;
	}
	CreateGameMap();
	CreateGameElements();
	CreateSnakeActor();
	SnakeGameMode = EGameMode::GAME;
}

void APlayerPawnBase::SnakeDead()
{
	MainMenuWinLost = "You lost!";
	StartGameButtonLabel = "New game";
	SnakeGameMode = EGameMode::MAIN_MENU;
	bIsGameRestart = true;
	bIsNextLevel = false;
}

void APlayerPawnBase::DestroyGameElements()
{
	//foods
	TArray<AActor*> FoundFoods;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), FoodClass, FoundFoods);
	for (auto Food : FoundFoods)
		Food->Destroy();
	//bonuses
	TArray<AActor*> FoundBonuses;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), BonusClass, FoundBonuses);
	for (auto Bonus : FoundBonuses)
		Bonus->Destroy();
	//barriers
	TArray<AActor*> FoundWalls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), WallClass, FoundWalls);
	for (auto Wall : FoundWalls) {
		if (Cast<AWall>(Wall)->WallMesh->GetMaterial(0) == BarriersMaterial) {
			Wall->Destroy();
		}
	}
}
