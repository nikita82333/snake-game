// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	LastMoveDirection = EMovementDirection::DOWN;
	NewMoveDirection = LastMoveDirection;
	bPassesThroughWalls = false;
	bPauseGame = false;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SnakePawn = GetWorld()->GetPlayerControllerIterator()->Get(0)->GetPawn<APlayerPawnBase>();
	ElementSize = SnakePawn->SnakeElementSize;
	ElementSpace = SnakePawn->SnakeElementSpace;
	MovementSpeed = SnakePawn->SnakeMovementSpeed;
	StartLenght = SnakePawn->SnakeStartLenght;
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(StartLenght);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (SnakePawn->SnakeGameMode == EGameMode::GAME)
		Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation(SnakeElements.Num() * (ElementSize + ElementSpace), 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->MeshComponent->SetVisibility(false);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
			NewSnakeElem->MeshComponent->SetVisibility(true);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedDelta = ElementSize + ElementSpace;
	if ((NewMoveDirection == EMovementDirection::UP && LastMoveDirection == EMovementDirection::DOWN) ||
		(NewMoveDirection == EMovementDirection::DOWN && LastMoveDirection == EMovementDirection::UP) ||
		(NewMoveDirection == EMovementDirection::LEFT && LastMoveDirection == EMovementDirection::RIGHT) ||
		(NewMoveDirection == EMovementDirection::RIGHT && LastMoveDirection == EMovementDirection::LEFT))
	{
		NewMoveDirection = LastMoveDirection;
	}
		
	switch (NewMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeedDelta;
		break;
	}
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		if (!SnakeElements[i]->MeshComponent->GetVisibleFlag())
			SnakeElements[i]->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	LastMoveDirection = NewMoveDirection;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::ScoreIncrement()
{
	SnakePawn->Score++;
	SnakePawn->TotalScore++;
	if (SnakePawn->Score == SnakePawn->FoodsNumber) {
		SnakePawn->MainMenuWinLost = "Level passed!";
		SnakePawn->StartGameButtonLabel = "Next level";
		SnakePawn->SnakeGameMode = EGameMode::MAIN_MENU;
		SnakePawn->bIsNextLevel = true;
		SnakePawn->bIsGameRestart = true;
	}
}

void ASnakeBase::DestroySnake()
{
	for (auto Element : SnakeElements)
		Element->Destroy();
	this->Destroy();
}
