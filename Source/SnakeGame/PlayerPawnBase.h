// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class AFood;
class AWall;
class ABonus;
class UCameraComponent;
class ASnakeBase;

enum class EMapElementType
{
	NONE,
	FOOD,
	WALL,
	B_SPEED_UP,
	B_SPEED_DOWN,
	B_THROUGH_WALLS,
	SNAKE
};

UENUM(BlueprintType)
enum class EGameMode : uint8 {
	MAIN_MENU = 0 UMETA(DisplayName = "Main menu"),
	GAME = 1 UMETA(DisplayName = "Game"),
	PAUSE = 2 UMETA(DisplayName = "Pause")
};

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	~APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EGameMode SnakeGameMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int Score;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int TotalScore;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int Level;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString MainMenuWinLost;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString StartGameButtonLabel;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString GameInfo;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWall> WallClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonus> BonusClass;

	UPROPERTY(EditDefaultsOnly)
	class UMaterial* SpeedUpMaterial;

	UPROPERTY(EditDefaultsOnly)
	class UMaterial* SpeedDownMaterial;

	UPROPERTY(EditDefaultsOnly)
	class UMaterial* ThroughWallsMaterial;

	UPROPERTY(EditDefaultsOnly)
	class UMaterial* BarriersMaterial;

	UPROPERTY(EditDefaultsOnly)
	float SnakeElementSize;

	UPROPERTY(EditDefaultsOnly)
	float SnakeElementSpace;

	UPROPERTY(EditDefaultsOnly)
	float SnakeMovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	int SnakeStartLenght;

	UPROPERTY(EditDefaultsOnly)
	int FieldSizeX;

	UPROPERTY(EditDefaultsOnly)
	int FieldSizeY;

	UPROPERTY(EditDefaultsOnly)
	int BaseWallsNumber;

	UPROPERTY(EditDefaultsOnly)
	int BaseFoodsNumber;

	UPROPERTY(EditDefaultsOnly)
	int BaseBonusesNumber;

	int WallsNumber;
	int FoodsNumber;
	int BonusesNumber;
	bool bIsNextLevel;
	bool bIsGameRestart;

private:
	EMapElementType** Map;

	void CreateGameMap();
	void AddWall(float X, float Y, float Z, bool bIsBarrier);
	void AddFood(float X, float Y);
	void AddBonus(float X, float Y, UMaterial* Material);
	void CreateGameElements();
	void CreateWalls();
	void GetRandomEmptyCell(int& X, int& Y);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION(BlueprintCallable)
	void StartGame();

	void SnakeDead();

	void DestroyGameElements();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlePlayerPauseInput(float value);
};
